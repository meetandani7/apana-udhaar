package com.example.prince.splitwise;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MyAccount extends AppCompatActivity {

    private EditText etname;
    private EditText etphone;
    private Button update;
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        etname = findViewById(R.id.name);
        etphone = findViewById(R.id.phone);
        update = findViewById(R.id.update);
        save = findViewById(R.id.save1);

        etname.setEnabled(false);
        etphone.setEnabled(false);

        final String[] phone1 = {""};

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        phone1[0] = (String) dataSnapshot.child("phonenumber1").getValue();
                        etphone.setText(phone1[0]);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("TAG", "getUser:onCancelled", databaseError.toException());
                    }
                });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etname.setEnabled(true);
                etphone.setEnabled(true);
            }
        });

    }
}
