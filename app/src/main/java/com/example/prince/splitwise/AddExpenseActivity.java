package com.example.prince.splitwise;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddExpenseActivity extends AppCompatActivity {

    private EditText et1;
    private EditText et2;
    private Button save;
    private Button add;
    static final int PICK_CONTACT = 1;
    int k=0,k1=0;
    private DatabaseReference databaseReference;
    public static final String ACCOUNT_SID = "AC3af89d072a0adef5f69385b59234194a";
    public static final String AUTH_TOKEN = "Show";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);

        et1 = findViewById(R.id.addname);
        et2 = findViewById(R.id.addexpense);
        save = findViewById(R.id.btnsave);
        add = findViewById(R.id.addfriend);

        et1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference ref=FirebaseDatabase.getInstance().getReference("PhoneBook").child(et1.getText().toString());
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        DatabaseReference d1=FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("PassBook");
                        final String x=d1.push().getKey();
                        d1.child(x).setValue(new Transaction(dataSnapshot.getKey(),"+"+et2.getText().toString()));
                        DatabaseReference d2=FirebaseDatabase.getInstance().getReference("USER").child(dataSnapshot.getValue().toString()).child("PassBook");
                        final String y=d2.push().getKey();
                        d2.child(y).setValue(new Transaction(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber(),"-"+et2.getText().toString()));
                        final DatabaseReference d3=FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("totalbalance");
                        final String[] x2 = {""};
                        final String[] x1 = {""};
                        k=0;
                        d3.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                x1[0] =dataSnapshot.getValue().toString();
                                x2[0] = x1[0].substring(1);
                                int bal=Integer.parseInt(x2[0]);

                                if (x1[0].charAt(0) == '+' && k==0) {
                                    bal = bal + Integer.parseInt(et2.getText().toString());
                                    d3.setValue("+"+bal);
                                    k=1;

                                }
                                else {
                                    if ( Integer.parseInt(x2[0]) > Integer.parseInt(et2.getText().toString()) && k==0){
                                        bal = bal - Integer.parseInt(et2.getText().toString());
                                        d3.setValue("-"+bal);
                                    }else if(Integer.parseInt(x2[0]) < Integer.parseInt(et2.getText().toString()) && k==0){
                                        bal = Integer.parseInt(et2.getText().toString())-bal;
                                        d3.setValue("+"+bal);
                                    }
                                    k=1;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        final DatabaseReference d4=FirebaseDatabase.getInstance().getReference("USER").child(dataSnapshot.getValue().toString()).child("totalbalance");
                        final String[] y2 = {""};
                        final String[] y1 = {""};
                        k1=0;
                        d4.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                y1[0] =dataSnapshot.getValue().toString();
                                y2[0] = y1[0].substring(1);
                                int bal=Integer.parseInt(y2[0]);

                                if (y1[0].charAt(0) == '+' && k1==0) {
                                    if ( Integer.parseInt(y2[0]) > Integer.parseInt(et2.getText().toString()) && k1==0){
                                        bal = bal - Integer.parseInt(et2.getText().toString());
                                        d4.setValue("+"+bal);
                                        k1=1;
                                    }else if(Integer.parseInt(y2[0]) < Integer.parseInt(et2.getText().toString()) && k1==0){
                                        bal = bal - Integer.parseInt(et2.getText().toString());
                                        if (bal<0)
                                            bal=-bal;
                                        d4.setValue("-"+bal);
                                        k1=1;
                                    }

                                }
                                else if(y1[0].charAt(0) == '-' && k1==0){

                                    bal = bal + Integer.parseInt(et2.getText().toString());
                                    d4.setValue("-"+bal);
                                    k1=1;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
        

    }
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();

                    String contactName = null;
                    String contactNumber = null;

                    // getting contacts ID
                    Cursor c = getContentResolver().query(contactData,null,null, null, null);
                    c.moveToNext();


                    String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    try {
                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            contactNumber = phones.getString(phones.getColumnIndex("data1"));
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                    contactName = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                    contactNumber = contactNumber.trim();
                    String s = "";
                    int count = 0;
                    for(int i=0; i<contactNumber.length(); i++)
                    {
                        if(contactNumber.charAt(contactNumber.length()-i-1)!=32){
                            s = s + contactNumber.charAt(contactNumber.length()-i-1);
                            count = count + 1;
                            if(count == 10)
                                break;
                        }
                    }

                    StringBuffer sb = new StringBuffer(s);
                    sb.reverse();
                    final String s1 = new String("+91"+sb);
                    final String[] ss = new String[1];
                    DatabaseReference ref=FirebaseDatabase.getInstance().getReference("PhoneBook").child(s1);
                    ref.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("abc","............"+dataSnapshot.getValue().toString());
                       ss[0] =dataSnapshot.getValue().toString();
                            DatabaseReference d1=FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("MyFriends");
                            final String x=d1.push().getKey();
                            d1.child(x).setValue(ss[0]);
                            DatabaseReference d2=FirebaseDatabase.getInstance().getReference("USER").child(ss[0]).child("MyFriends");
                            final String y=d2.push().getKey();
                            d2.child(y).setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                     /*  DatabaseReference d1=FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("MyFriends");
                    final String x=d1.push().getKey();
                    d1.child(x).setValue(a.get(0));
                    DatabaseReference d2=FirebaseDatabase.getInstance().getReference("USER").child(""+a.get(0)).child("MyFriends");
                    final String y=d2.push().getKey();
                    d1.child(y).setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    */
//                    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
//
//                    Message message = Message
//                            .creator(new PhoneNumber("+917433074500"), // to
//                                    new PhoneNumber("+12013748709"), // from
//                                    "Where's Wallace?")
//                            .create();
//
//                    System.out.print(message.getSid());
                }
                break;
            case (2):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();

                    String contactName = null;
                    String contactNumber = null;

                    // getting contacts ID
                    Cursor c = getContentResolver().query(contactData,null,null, null, null);
                    c.moveToNext();


                    String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    try {
                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            contactNumber = phones.getString(phones.getColumnIndex("data1"));
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                    contactName = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                    contactNumber = contactNumber.trim();
                    String s = "";
                    int count = 0;
                    for(int i=0; i<contactNumber.length(); i++)
                    {
                        if(contactNumber.charAt(contactNumber.length()-i-1)!=32){
                            s = s + contactNumber.charAt(contactNumber.length()-i-1);
                            count = count + 1;
                            if(count == 10)
                                break;
                        }
                    }

                    StringBuffer sb = new StringBuffer(s);
                    sb.reverse();
                    final String s1 = new String("+91"+sb);
                    et1.setText(s1);

                    break;
        }
    }
}
}

//public class SmsSender {
//    // Find your Account Sid and Auth Token at twilio.com/console
//    public static final String ACCOUNT_SID =
//            "AC3af89d072a0adef5f69385b59234194a";
//    public static final String AUTH_TOKEN =
//            "Show";
//
//    public static void main(String[] args) {
//        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
//
//        Message message = Message
//                .creator(new PhoneNumber("+14159352345"), // to
//                        new PhoneNumber("+12013748709"), // from
//                        "Where's Wallace?")
//                .create();
//
//    }
//}