package com.example.prince.splitwise;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class tab1 extends Fragment {
    FloatingActionButton menu2,menu3,menu4 ;
    TextView tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tab1, container, false);

        menu2 = v.findViewById(R.id.subFloatingMenu1) ;
        menu3 = v.findViewById(R.id.subFloatingMenu2) ;
        menu4 = v.findViewById(R.id.subFloatingMenu3) ;

        tv=v.findViewById(R.id.tv);
        DatabaseReference dr=FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("totalbalance");
        dr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String total = dataSnapshot.getValue(String.class);
                tv.setText(total);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), AddExpenseActivity.class);
                startActivity(intent);
            }
        });

        menu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext() , "BackUp Icon clicked", Toast.LENGTH_LONG).show();

            }
        });

        menu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext() , "Settings Icon clicked", Toast.LENGTH_LONG).show();

            }
        });
        return v;
    }
}