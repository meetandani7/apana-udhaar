package com.example.prince.splitwise;

public class PhoneBook {
    String myuid;

    public PhoneBook(String myuid) {
        this.myuid = myuid;
    }

    public PhoneBook() {
    }

    public String getMyuid() {
        return myuid;
    }

    public void setMyuid(String myuid) {
        this.myuid = myuid;
    }
}
