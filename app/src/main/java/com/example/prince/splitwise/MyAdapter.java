package com.example.prince.splitwise;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {
    Context context;
    ArrayList<Transaction> list;
    LayoutInflater inflater;

    public MyAdapter(Context context, ArrayList<Transaction> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);

   }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.transaction_layout,null);
        TextView pt = view.findViewById(R.id.phone_transaction);
        TextView pay = view.findViewById(R.id.payment_transaction);

        Transaction t = new Transaction();
        t = list.get(i);
        pt.setText(t.getT_uid());
        pay.setText(t.getT_payment());


        return view;
    }
}
