package com.example.prince.splitwise;

public class Transaction{
    private String t_uid,t_payment;

    public Transaction() {
    }

    public Transaction(String t_uid, String t_payment) {
        this.t_uid = t_uid;
        this.t_payment = t_payment;
    }

    public String getT_uid() {
        return t_uid;
    }

    public void setT_uid(String t_uid) {
        this.t_uid = t_uid;
    }

    public String getT_payment() {
        return t_payment;
    }

    public void setT_payment(String t_payment) {
        this.t_payment = t_payment;
    }
}
