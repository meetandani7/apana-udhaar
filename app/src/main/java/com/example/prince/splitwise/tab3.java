package com.example.prince.splitwise;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class tab3 extends Fragment {
    MyAdapter adapter;
    private ListView lv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tab3, container, false);
        lv=v.findViewById(R.id.lv);


        return v;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        printPassbook(context);
    }

    private void printPassbook(final Context context) {


        DatabaseReference dr=FirebaseDatabase.getInstance().getReference("USER").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("PassBook");
        dr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final ArrayList<Transaction> tlist;
                tlist=new ArrayList<>();
                for (DataSnapshot d:dataSnapshot.getChildren())
                {
                    tlist.add(d.getValue(Transaction.class));
                }
                adapter=new MyAdapter(context,tlist);
                lv.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}